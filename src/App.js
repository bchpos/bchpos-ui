import React, { Component } from 'react';
import { Icon, Navbar } from 'react-materialize'
import './App.css';

import { HashRouter, NavLink, Route } from "react-router-dom";
import Home from "./Home";
import Order from "./Order";
import Payment from "./Payment";
import Ledger from "./Ledger";
import Settings from "./Settings";

class App extends Component {
    constructor(props) {
        super(props);

        let shopName;
        let validConfiguration = true;

        if (localStorage.getItem('name'))
            shopName = localStorage.getItem('name');
        else
            shopName = "Bitcoin Cash Store";
        localStorage.setItem('name', shopName);

        if (localStorage.getItem('outlet') == null || localStorage.getObj('outlets') == null) {
            validConfiguration = false;
        }

        this.state = {
            name: shopName,
            validConfiguration: validConfiguration
        };
    }


    render() {
        return (
            <HashRouter>
                <div className="App">
                    <Navbar left brand={this.state.name} className="green home-brand">
                        <li><NavLink to="/"><Icon left>home</Icon>INICIO</NavLink></li>
                        {this.state.validConfiguration ?
                            <li><NavLink to="/order"><Icon left>shopping_cart</Icon>ORDEN</NavLink></li> : null}
                        {this.state.validConfiguration ?
                            <li><NavLink to="/payment"><Icon left>payment</Icon>PAGO</NavLink></li> : null}
                        {this.state.validConfiguration ?
                            <li><NavLink to="/ledger"><Icon left>list</Icon>HISTORIAL</NavLink></li> : null}
                        <li><NavLink to="/settings"><Icon left>settings</Icon>CONFIGURACION</NavLink></li>
                    </Navbar>

                    <Route exact path="/" component={Home}/>
                    <Route path="/order" component={Order}/>
                    <Route path="/payment" component={Payment}/>
                    <Route path="/ledger" component={Ledger}/>
                    <Route path="/settings" component={Settings}/>

                </div>
            </HashRouter>
        );
    }
}

Storage.prototype.setObj = function (key, obj) {
    return this.setItem(key, JSON.stringify(obj))
}
Storage.prototype.getObj = function (key) {
    return JSON.parse(this.getItem(key))
}

export default App;
