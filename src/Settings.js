import React, { Component, Fragment } from "react";
import { Button, Col, Container, Icon, Input, Row } from 'react-materialize'
import { v4 } from 'uuid';
import { NavLink, } from "react-router-dom";
import ScaleLoader from "react-spinners/ScaleLoader"
import PingPong from "./PingPong";

class Settings extends Component {
    constructor(props) {
        super(props);
        let server;
        let shopName;
        let source;
        let currency;
        let tax;
        let discount;
        let outlet;
        let online;

        if (localStorage.getItem('server'))
            server = localStorage.getItem('server');
        else
            server = "http://localhost:8080";
        localStorage.setItem('server', server);

        if (localStorage.getItem('name'))
            shopName = localStorage.getItem('name');
        else
            shopName = "Demo Store";

        if (localStorage.getItem('source'))
            source = localStorage.getItem('source');
        else
            source = "bitso";

        if (localStorage.getItem('currency'))
            currency = localStorage.getItem('currency');
        else
            currency = "MXN";

        if (localStorage.getItem('tax')) {
            //console.log(localStorage.getItem('tax'));
            tax = JSON.parse(localStorage.getItem('tax'))
        } else {
            tax = [
                {
                    id: v4(),
                    value: "0"
                }
            ]
        }

        if (localStorage.getItem('discount')) {
            discount = JSON.parse(localStorage.getItem('discount'))
        } else {
            discount = [
                {
                    id: v4(),
                    value: "0"
                }
            ]
        }

        if (localStorage.getItem('outlet'))
            outlet = localStorage.getItem('outlet');
        else
            outlet = 'bch'

        online = !!localStorage.getItem('online');

        this.state = {
            server: server,
            name: shopName,
            currency: currency,
            source: source,
            currencies: [currency],
            sources: [source],
            cancel: "Cerrar",
            save: "Guardar",
            wipe: "Limpiar",
            submitted: true,
            wiped: false,
            tax: tax,
            discount: discount,
            outlet: outlet,
            outlets: null,
            serverError: false,
            online: online
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handleServer = this.handleServer.bind(this);
        this.handleWipe = this.handleWipe.bind(this);
        this.handleSource = this.handleSource.bind(this);
        this.handleCurrency = this.handleCurrency.bind(this);
        this.queryCurrencies = this.queryCurrencies.bind(this);
        this.handleTax = this.handleTax.bind(this);
        this.handleTaxPlus = this.handleTaxPlus.bind(this);
        this.handleTaxMin = this.handleTaxMin.bind(this);
        this.handleDiscount = this.handleDiscount.bind(this);
        this.handleDiscountPlus = this.handleDiscountPlus.bind(this);
        this.handleDiscountMin = this.handleDiscountMin.bind(this);
        this.handleOutlet = this.handleOutlet.bind(this);
        this.queryOutlets = this.queryOutlets.bind(this);
        this.handleField = this.handleField.bind(this);
    }

    async componentDidMount() {
        const query = this.state.server + "/api/rate";
        fetch(query)
            .then(results => results.json())
            .then(json => {
                let data = [];
                const keys = Object.keys(json['sources']);
                for (const key of keys) {
                    data.push(json['sources'][key]);
                }
                if (!this.state.source)
                    this.setState({sources: data, source: data[0]});
                else
                    this.setState({sources: data})
            });

        this.queryCurrencies();
        this.queryOutlets();
    }


    queryCurrencies() {
        const query = this.state.server + "/api/rate?source=" + this.state.source;
        fetch(query)
            .then(results => results.json())
            .then(json => {
                let data = [];
                const keys = Object.keys(json['currencies']);
                for (const key of keys) {
                    data.push(json['currencies'][key]);
                }
                if (!this.state.currency)
                    this.setState({currencies: data, currency: data[0]});
                else
                    this.setState({currencies: data})
            })
            .catch(error => {
                console.log("¡El servidor no responde! :/", error);
                this.setState({online: false})
            });
    }

    queryOutlets() {
        const query = this.state.server + "/bitso/outlet";
        fetch(query)
            .then(results => results.json())
            .then(json => {
                this.setState({outlets: json.outlets})
                localStorage.setObj('outlets', json.outlets);
                let selectedOutlet = json.outlets.find(outlet => outlet.id === this.state.outlet);
                if (selectedOutlet === undefined) {
                    this.setState({outlet: json.outlets[0].id})
                }
            })
            .catch(error => {
                console.log("¡El servidor no responde! :/", error);
                this.setState({online: false})
            });
    }

    handleWipe(event) {
        event.preventDefault();
        localStorage.clear();
        this.setState({
            wipe: "¡Limpiado!",
            wiped: true,
            save: "Guardar",
            submitted: false,
        });
        window.Materialize.toast('<b>¡Se han limpiado los datos locales!</b><br />', 4000, 'orange')
    }

    handleSubmit() {
        if (this.state.name && this.state.server) {
            localStorage.setItem('name', this.state.name);
            localStorage.setItem('server', this.state.server);
            localStorage.setItem('source', this.state.source);
            localStorage.setItem('currency', this.state.currency);
            localStorage.setItem('tax', JSON.stringify(this.state.tax));
            localStorage.setItem('discount', JSON.stringify(this.state.discount));
            localStorage.setItem('outlet', JSON.stringify(this.state.outlet));

            this.setState({
                save: "¡Guardado!",
                submitted: true,
                wipe: "Limpiar",
                wiped: false,
                cancel: "Cerrar",
            });

            window.Materialize.toast('<b>¡Configuración guardada!</b><br />', 4000, 'green')
        }
    }

    handleName(event) {
        event.preventDefault();
        //console.log(event.target.value);
        localStorage.setItem('name', event.target.value);
        this.setState({
            name: event.target.value,
            save: "Guardar",
            wipe: "Limpiar",
            submitted: false,
            wiped: false,
        })
    }

    handleServer(event) {
        event.preventDefault();
        localStorage.setItem('server', event.target.value);
        this.setState({
            server: event.target.value,
            save: "Guardar",
            wipe: "Limpiar",
            submitted: false,
            wiped: false,
        })
    }

    handleSource(event) {
        event.preventDefault();
        //console.log(event.target.value)
        localStorage.setItem('source', event.target.value);
        this.setState({
            source: event.target.value,
            currencies: [],
            save: "Guardar",
            wipe: "Limpiar",
            submitted: false,
            wiped: false,
        });

        this.queryCurrencies()
    }

    handleCurrency(event) {
        event.preventDefault();
        localStorage.setItem('currency', event.target.value);
        this.setState({
            currency: event.target.value,
            save: "Guardar",
            wipe: "Limpiar",
            submitted: false,
            wiped: false,
        })
    }

    handleTax(id, event) {
        event.preventDefault();

        let items = this.state.tax;

        let value = event.target.value;

        if (/^\d+$/.test(value)) {
            if (value >= 0 && value <= 100) {
                items[id]['value'] = value;
                localStorage.setItem('tax', JSON.stringify(items));
                this.setState({
                    tax: items,
                    save: "Guardar",
                    wipe: "Limpiar",
                    submitted: false,
                    wiped: false,
                })
            }
        }
    }

    handleDiscount(id, event) {
        event.preventDefault();

        let items = this.state.discount;

        let value = event.target.value;

        if (/^\d+$/.test(value)) {
            if (value >= 0 && value <= 100) {
                items[id]['value'] = value;
                localStorage.setItem('discount', JSON.stringify(items));
                this.setState({
                    discount: items,
                    save: "Guardar",
                    wipe: "Limpiar",
                    submitted: false,
                    wiped: false,
                })
            }
        }
    }

    handleTaxPlus(id, event) {
        event.preventDefault();

        let items = this.state.tax;
        items.push({
            id: v4(),
            value: "",
        });

        this.setState({
            tax: items,
            save: "Guardar",
            wipe: "Limpiar",
            submitted: false,
            wiped: false,
        })
    }

    handleTaxMin(id, event) {
        event.preventDefault();
        console.log(id);
        console.log(this.state.tax);

        let items = this.state.tax.filter((item, key) => key !== id);
        console.log(items);
        if (items.length === 0)
            items = [{
                id: v4(),
                value: "0",
            }];

        localStorage.setItem('tax', JSON.stringify(items));
        this.setState({
            tax: items,
            save: "Guardar",
            wipe: "Limpiar",
            submitted: false,
            wiped: false,
        })
    }

    handleDiscountPlus(id, event) {
        event.preventDefault();

        let items = this.state.discount;
        items.push({
            id: v4(),
            value: "",
        });

        this.setState({
            discount: items,
            save: "Guardar",
            wipe: "Limpiar",
            submitted: false,
            wiped: false,
        })
    }

    handleDiscountMin(id, event) {
        event.preventDefault();
        console.log(id);
        console.log(this.state.tax);

        let items = this.state.discount.filter((item, key) => key !== id);
        console.log(items);
        if (items.length === 0)
            items = [{
                id: v4(),
                value: "0",
            }];

        localStorage.setItem('discount', JSON.stringify(items));
        this.setState({
            discount: items,
            save: "Guardar",
            wipe: "Limpiar",
            submitted: false,
            wiped: false,
        })
    }

    handleOutlet(event) {
        event.preventDefault();
        localStorage.setItem('outlet', event.target.value);
        this.setState({
            outlet: event.target.value,
            save: "Guardar",
            wipe: "Limpiar",
            submitted: false,
            wiped: false,
        });

        this.queryOutlets();
    }

    handleField(event) {
        event.preventDefault();
        console.log(event.target);
    }

    moveCaretAtEnd(e) {
        const temp_value = e.target.value;
        e.target.value = '';
        e.target.value = temp_value
    }

    render() {
        if (!this.state.online) {
            return (
                <Container>
                    <PingPong/>
                    <h3>Configuración</h3>
                    <form>
                        <Col s={12}>
                            <Row>
                                <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">
                                    <Input s={12} label="Server" required validate defaultValue={this.state.server}
                                           onChange={this.handleServer.bind(this)}/>
                                </Col>
                            </Row>
                        </Col>
                    </form>
                </Container>
            );
        } else if (!this.state.outlets) {
            return (
                <h3>
                    <Container>
                        <ScaleLoader className="target-ratio-resize"
                                     size={3000}
                                     color={"green"}
                        />
                    </Container>
                </h3>
            )
        } else {
            return (
                <Container>
                    <PingPong/>
                    <h3>Configuración</h3>
                    <form>
                        <Col s={12}>
                            <Row>
                                <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">
                                    <Input s={12} label="Nombre de la tienda" required validate
                                           defaultValue={this.state.name} onChange={this.handleName.bind(this)}/>
                                </Col>
                            </Row>
                            <Row>
                                <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">
                                    <Input s={12} label="Server" required validate defaultValue={this.state.server}
                                           onChange={this.handleServer.bind(this)}/>
                                </Col>
                            </Row>
                            <Row>
                                <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">
                                    <Input s={8} type='select' label='Fuente de precios de intercambio'
                                           defaultValue={this.state.source}
                                           onChange={this.handleSource.bind(this)}
                                           onClick={e => e.stopPropagation()}>
                                        {this.state.sources.map(function (item) {
                                            return (
                                                <option key={item}
                                                        value={item}>{item.charAt(0).toUpperCase() + item.slice(1)}</option>
                                            )
                                        })}
                                    </Input>
                                    <Input s={4} type='select' label='Divisa' defaultValue={this.state.currency}
                                           onChange={this.handleCurrency.bind(this)}
                                           onClick={e => e.stopPropagation()}>
                                        {this.state.currencies.map(function (item) {
                                            return (
                                                <option key={item} value={item}>{item}</option>
                                            )
                                        })}
                                    </Input>
                                </Col>
                            </Row>
                            <Row>
                                <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">
                                    <Input s={12} type='select' label="Salida de pagos" required validate
                                           value={this.state.outlet}
                                           onClick={e => e.stopPropagation()}
                                           onChange={this.handleOutlet.bind(this)}>
                                        {Object.entries(this.state.outlets).map(function ([key, value]) {
                                            return (
                                                <option key={key} value={value.id}>{value.name}</option>
                                            )
                                        })}
                                    </Input>
                                </Col>
                            </Row>
                            <OutletFields
                                outlet={this.state.outlet}
                                outlets={this.state.outlets}
                                handleField={this.handleField.bind(this)}/>
                            <Row>
                                <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">

                                    <Col s={12} l={6} xl={6}>
                                        <h5 className="presetsh5">Impuestos (%)</h5>
                                        {this.state.tax.map((item, key) => {
                                            let label = key === 0 ? "Predeterminado" : "Impuesto " + (key + 1);
                                            return (
                                                <div className="presets" key={item.id}>
                                                    <Col s={4} l={4} xl={4}>
                                                        <label className="left" htmlFor={item.id}>{label}</label>
                                                        <input key={item.id} id={item.id} required
                                                               defaultValue={item.value}
                                                               onChange={this.handleTax.bind(this, key)}/>
                                                    </Col>
                                                    <Col s={7} l={7} xl={7} className="offset-l0 offset-xl0">
                                                        <div className="input-buttons">
                                                            <Button floating waves='light' className="red input-btn"
                                                                    onClick={this.handleTaxMin.bind(this, key)}><Icon>remove</Icon></Button>
                                                            {this.state.tax.length === (key + 1) ?
                                                                <Button floating waves='light'
                                                                        className="green input-btn"
                                                                        onClick={this.handleTaxPlus.bind(this, key)}><Icon>add</Icon></Button> : ""}
                                                        </div>
                                                    </Col>
                                                </div>
                                            );
                                        })}
                                    </Col>

                                    <Col s={12} l={6} xl={6}>
                                        <h5 className="presetsh5">Descuentos (%)</h5>
                                        {this.state.discount.map((item, key) => {
                                            let label = key === 0 ? "Predeterminado" : "Descuento " + (key + 1);
                                            return (
                                                <div className="presets" key={item.id}>
                                                    <Col s={4} l={4} xl={4}>
                                                        <label className="left" htmlFor={item.id}>{label}</label>
                                                        <input key={item.id} id={item.id} required
                                                               defaultValue={item.value}
                                                               onChange={this.handleDiscount.bind(this, key)}/>
                                                    </Col>
                                                    <Col s={7} l={7} xl={7}>
                                                        <div className="input-buttons">
                                                            <Button floating waves='light' className="red input-btn"
                                                                    onClick={this.handleDiscountMin.bind(this, key)}><Icon>remove</Icon></Button>
                                                            {this.state.discount.length === (key + 1) ?
                                                                <Button floating waves='light'
                                                                        className="green input-btn"
                                                                        onClick={this.handleDiscountPlus.bind(this, key)}><Icon>add</Icon></Button> : ""}
                                                        </div>
                                                    </Col>
                                                    <br/>
                                                </div>
                                            );
                                        })}
                                    </Col>

                                </Col>
                            </Row>
                            <Row>
                                <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">

                                </Col>
                                <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">

                                </Col>
                            </Row>
                            <Row>
                                <Col s={4} l={3} xl={3} className="offset-s4 offset-l3">
                                    <NavLink to="/"><Button large waves='light' className="red left bold-big"><Icon
                                        left>close</Icon>{this.state.cancel}</Button></NavLink>
                                </Col>
                                <Col s={4} l={3} xl={3}>
                                    <Button large waves='light' className="orange right bold-big"
                                            disabled={this.state.wiped} onClick={this.handleWipe.bind(this)}><Icon
                                        left>delete_forever</Icon>{this.state.wipe}</Button>
                                </Col>
                            </Row>
                            <Row>
                                <Col s={4} l={3} xl={3} className="offset-s8 offset-l6">

                                </Col>
                            </Row>
                        </Col>
                    </form>
                </Container>
            );
        }

    }
}

class OutletFields extends Component {
    constructor(props) {
        super(props);
        console.log(props);
    }

    render() {
        let outlet;
        let requiredFields;
        let optionalFields;

        if (this.props.outlet != null) {
            outlet = this.props.outlets.find(o => o.id === this.props.outlet)
        }

        if (outlet != null && outlet.required_fields.length > 0) {
            requiredFields = <Fragment>
                <h3>Campos requeridos</h3>
                {outlet.required_fields.map(field => {
                    return <Row key={field.id.toString()}>
                        <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">
                            <Input
                                id={field.id}
                                s={12}
                                label={field.name}
                                defaultValue={localStorage.getItem(field.id) || undefined}
                                required
                                validate
                                onChange={(e) => localStorage.setItem(field.id, e.target.value)}
                            />
                        </Col>
                    </Row>
                })}
            </Fragment>
        }
        if (outlet != null && outlet.optional_fields.length > 0) {
            optionalFields = <Fragment>
                <h3>Campos opcionales</h3>
                {outlet.optional_fields.map(field => {
                    return <Row key={field.id.toString()}>
                        <Col s={12} l={6} xl={6} className="offset-l3 offset-xl3">
                            <Input
                                id={field.id}
                                s={12}
                                label={field.name}
                                defaultValue={localStorage.getItem(field.id) || undefined}
                                validate
                                onChange={(e) => localStorage.setItem(field.id, e.target.value)}
                            />
                        </Col>
                    </Row>
                })}
            </Fragment>
        }

        return (
            <div>
                {requiredFields}
                {optionalFields}
            </div>
        )
    }
}

export default Settings;
 
