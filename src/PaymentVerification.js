import React, { Component } from "react";
import { CardPanel, Col, Row } from 'react-materialize'

class PaymentVerification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            server: localStorage.getItem('server'),
            addr: props.addr,
            amount: props.amount,
            received: 0,
        };
        this.fetchVerify = this.fetchVerify.bind(this);
    }

    componentDidMount() {
        const intervalId = setInterval(() => this.fetchVerify(), 4000);
        this.setState({intervalId: intervalId});

    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId);
    }

    async fetchVerify() {
        if (!this.state.received) {
            const query = this.state.server + "/api/verify?addr=" + this.state.addr + "&amount=" + this.state.amount;

            fetch(query)
                .then(d => d.json())
                .then(d => {
                    this.setState({received: d['received']});
                    if (d['received'] === 1) {
//                         window.Materialize.toast('<b>PAYMENT RECEIVED!</b><br />', 4000 , 'green');
//                         this.props.isPaid(true);
                    }
                })
        } else {
            window.Materialize.toast('<b>¡PAGO RECIBIDO!</b><br />', 4000, 'green');
            this.props.isPaid(true)
        }
    }

    render() {
        if (!this.state.received) {
            return (
                <Row>
                    <Col s={12} l={6} className="offset-l3">
                        <CardPanel className="orange white-text">
              <span>
                <h2>Esperando...<br/>Expira en: {this.props.expires}s</h2>
              </span>
                        </CardPanel>
                    </Col>
                </Row>
            );
        } else {
            return (
                <Row>
                    <Col s={12} l={6} className="offset-l3">
                        <CardPanel className="green white-text">
              <span>
                <h3>¡RECIBIDO!</h3>
              </span>
                        </CardPanel>
                    </Col>
                </Row>
            );
        }
    }
}

export default PaymentVerification; 
 
