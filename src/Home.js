import React, { Component } from "react";
import { Button, CardPanel, Col, Container, Icon, Row } from 'react-materialize';
import PingPong from './PingPong';
import { v4 } from 'uuid';
import { HashRouter, NavLink } from "react-router-dom";

class Home extends Component {
    constructor(props) {
        super(props);

        let server;
        let shopName;
        let source;
        let currency;
        let tax;
        let discount;

        if (localStorage.getItem('server'))
            server = localStorage.getItem('server');
        else
            server = "http://localhost:8080";

        if (localStorage.getItem('name'))
            shopName = localStorage.getItem('name');
        else
            shopName = "Demo";

        if (localStorage.getItem('source'))
            source = localStorage.getItem('source');
        else
            source = "bitso";

        if (localStorage.getItem('currency'))
            currency = localStorage.getItem('currency');
        else
            currency = "MXN";

        if (localStorage.getItem('tax'))
            tax = JSON.parse(localStorage.getItem('tax'));
        else
            tax = [
                {
                    id: v4(),
                    value: "0"
                }
            ];

        if (localStorage.getItem('discount'))
            discount = JSON.parse(localStorage.getItem('discount'));
        else
            discount = [
                {
                    id: v4(),
                    value: "0"
                }
            ];

        localStorage.setItem('server', server);
        localStorage.setItem('name', shopName);
        localStorage.setItem('source', source);
        localStorage.setItem('currency', currency);
        localStorage.setItem('tax', JSON.stringify(tax));
        localStorage.setItem('discount', JSON.stringify(discount));

        this.state = {
            name: shopName,
            server: server,
            outlets: localStorage.getObj('outlets')
        };
    }

    componentWillMount() {
    }


    render() {
        let greeting;
        if (this.state.name && this.state.server && this.state.outlets) {
            greeting = (
                <Container>
                    <h1>¡{this.state.name}!</h1>
                    <CardPanel className="grey lighten-3 grey-text">
                        <Row>
                            <Col s={12} className='grid-index'><h3>Escoge una opción:</h3></Col>
                        </Row>
                        <Row>
                            <Col s={12} l={4} className='grid-index action'><NavLink to="/order"><Button large
                                                                                                         waves='light'
                                                                                                         className="green btn-action"><Icon
                                left>shopping_cart</Icon>ORDEN</Button></NavLink></Col>
                            <Col s={12} l={4} className='grid-index action'><NavLink to="/payment"><Button large
                                                                                                           waves='light'
                                                                                                           className="green btn-action"><Icon
                                left>payment</Icon>PAGO</Button></NavLink></Col>
                            <Col s={12} l={4} className='grid-index action'><NavLink to="/ledger"><Button large
                                                                                                          waves='light'
                                                                                                          className="green btn-action"><Icon
                                left>list</Icon>HISTORIAL</Button></NavLink></Col>
                        </Row>
                    </CardPanel>
                </Container>
            )
        } else {
            greeting = (
                <Container>
                    <Row>
                        <Col s={12} l={6} className="offset-l3">
                            <CardPanel className="red white-text">
              <span>
                <h4>¡Bienvenido! </h4>
                <span>Ve a 'Configuración' para agregar información de la tienda</span><br/><br/>
                <NavLink to="/settings"><Button large waves='light' className="blue lighten-1"
                                                icon="settings"/></NavLink>
              </span>
                            </CardPanel>
                        </Col>
                    </Row>
                </Container>
            )
        }

        return (
            <HashRouter>
                <Container>
                    <br/>{greeting}<br/>
                    <PingPong/>
                </Container>
            </HashRouter>
        );
    }
}

export default Home;
