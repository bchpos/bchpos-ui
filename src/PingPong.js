import React, { Component } from "react";
import { Container, Icon } from 'react-materialize';


class PingPong extends Component {
    constructor(props) {
        super(props);
        this.state = {
            server: localStorage.getItem('server'),
            pong: 0,
        };
        this.ping = this.ping.bind(this);
        this.handleErrors = this.handleErrors.bind(this);
    }

    componentDidMount() {
        this.ping();
        const intervalId = setInterval(() => this.ping(), 60000);
        this.setState({intervalId: intervalId});

    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId);
    }

    handleErrors(response) {
        if (!response.ok) {
            localStorage.removeItem('online')
            throw Error(response.statusText);
        } else {
            localStorage.setItem('online', true)
        }
        return response;
    }


    ping() {
        const query = this.state.server + "/api/ping";

        fetch(query)
            .then(this.handleErrors)
            .then(d => d.json())
            .then(response => this.setState({pong: response['pong']}))
            .catch(error => {
                console.log("¡El servidor no responde! :/", error);
                this.setState({pong: 0})
            });
    }

    render() {
        return (
            <Container>
                <span className="pingpong">{this.state.pong ? <Icon tiny className="green-text">check</Icon> :
                    <Icon tiny className="red-text">close</Icon>}
                    <b>Conexión al servidor:</b> {this.state.server}</span>
            </Container>
        );
    }
}

export default PingPong;
