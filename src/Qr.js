import React, { Component } from "react";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { CardPanel, Col, Container, Row } from 'react-materialize'
import PaymentVerification from "./PaymentVerification"
import { default as UUID } from "node-uuid";
import ScaleLoader from "react-spinners/ScaleLoader"

class Qr extends Component {

    constructor(props) {
        super(props);
        this.state = {
            server: localStorage.getItem('server'),
            outlet: localStorage.getItem('outlet'),
            outlets: localStorage.getObj('outlets'),
            amount: props.amount,
            bch: props.bch,
            label: props.label,
            currency: props.currency,
            copied: false,
            received: false,
            seconds: 60
        };
        this.handleCopy = this.handleCopy.bind(this);
        this.isPaid = this.isPaid.bind(this);
    }

    componentDidMount() {
        this.initialize_qr();
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    regenerate_qr() {
        this.setState({
            label: UUID.v4().slice(-12, -1),
            apiResult: null
        });
        this.initialize_qr();
    }

    initialize_qr() {
        if (this.state.outlet != null && this.state.outlets != null) {
            let query = this.state.server + "/api/payment?amount=" + this.state.bch + "&label=" + this.state.label;
            query += "&outlet=" + this.state.outlet;
            let outlet = this.state.outlets.find(o => o.id === this.state.outlet)

            outlet.required_fields.forEach((field) => {
                if (localStorage.getItem(field.id) != null) {
                    query += "&" + field.id + "=" + localStorage.getItem(field.id);
                }
            });
            outlet.optional_fields.forEach((field) => {
                if (localStorage.getItem(field.id) != null) {
                    query += "&" + field.id + "=" + localStorage.getItem(field.id);
                }
            });

            fetch(query)
                .then(d => d.json())
                .then(d => {
                    console.log(d);
                    this.setState({
                        apiResult: d,
                        qrImg: this.state.server + d['payment']['qr_img'],
                        addr: d['payment']['addr'],
                        paymentUri: d['payment']['payment_uri'],
                        explorerUrl: d['payment']['explorer_url'],
                        expires: d['payment']['expires']
                    })
                })
                .then(() => {
                    this.calculateCountdown(new Date(this.state.expires * 1000))
                });
        } else {
            window.Materialize.toast('<b>¡No se encontró salida de pagos! Revise configuración</b>', 5000, 'red')
        }
    }

    handleCopy() {
        this.setState({copied: true});
        window.Materialize.toast('¡URI de pago copiado!', 5000, 'green')
    }

    isPaid(state) {
        //event.preventDefault();
        console.log(state);
        this.setState({received: state})
    }

    getTwoDigitValue(value) {
        if (value < 10) {
            return "0" + value;
        }
        return "" + value;
    }

    calculateCountdown(endDate) {
        const startDate = new Date();
        const timeRemaining = endDate.getTime() - startDate.getTime();

        if (timeRemaining > 0) {
            const start_date = new Date(startDate);
            const end_date = new Date(endDate);
            const start_millis = start_date.getTime();
            const end_millis = end_date.getTime();

            const old_sec = start_millis / 1000;
            const current_sec = end_millis / 1000;

            let seconds = current_sec - old_sec;

            let days = Math.floor(seconds / (24 * 60 * 60));
            seconds -= days * 24 * 60 * 60;

            let hours = Math.floor(seconds / (60 * 60));
            seconds -= hours * 60 * 60;

            let minutes = Math.floor(seconds / 60);
            seconds -= minutes * 60;

            days = Math.abs(days);
            hours = Math.abs(hours);
            minutes = Math.abs(minutes);
            seconds = Math.floor(Math.abs(seconds));

            this.setState({
                    days: days,
                    hours: hours,
                    minutes: minutes,
                    seconds: seconds
                },
                () => {
                    //this.timer = setTimeout(this.calculateCountdown, 1000);
                    this.timer = setTimeout(() => this.calculateCountdown(endDate), 1000);
                }
            );
        } else {
            clearTimeout(this.timer);
            this.regenerate_qr();
        }
    }

    render() {
        if (!this.state.apiResult) {
            return (
                <h3>
                    <Container>
                        <ScaleLoader className="target-ratio-resize"
                                     size={3000}
                                     color={"green"}
                        />
                    </Container>
                </h3>
            )
        }

        if (!this.state.received) {
            return (
                <div>
                    <Container>
                        <h3>Pagar {this.state.bch} BCH</h3>
                        <CopyToClipboard text={this.state.paymentUri} onCopy={this.handleCopy.bind(this)}>
                            <span><img src={this.state.qrImg} alt={this.state.paymentUri}
                                       className="target-ratio-resize"/></span>
                        </CopyToClipboard>
                        <ul>
                            <li>Cantidad: {this.state.amount} {this.state.currency} - {this.state.bch} BCH</li>
                            <li>Dirección: <a target="_blank"
                                              href={this.state.explorerUrl + this.state.addr}>{this.state.addr}</a></li>

                            <li>Etiqueta: {this.state.label}</li>
                        </ul>
                        <PaymentVerification addr={this.state.addr} amount={this.state.bch} isPaid={this.isPaid}
                                             expires={this.state.seconds}/>
                    </Container>
                </div>
            );
        } else {
            return (
                <div>

                    <Row>
                        <Col s={12} l={6} className="offset-l3">
                            <CardPanel className="green white-text">
                <span>
                  <h3>PAGADO {this.state.bch} BCH!</h3>
                  <h4>¡Gracias!</h4>
                </span>
                            </CardPanel>
                        </Col>
                    </Row>

                    <ul>
                        <li>Cantidad: {this.state.amount} {this.state.currency} - {this.state.bch} BCH</li>
                        <li>Dirección: <a target="_blank"
                                          href={this.state.explorerUrl + this.state.addr}>{this.state.addr}</a></li>
                        <li>Etiqueta: {this.state.label}</li>
                    </ul>

                </div>
            );
        }
    }
}

export default Qr; 
